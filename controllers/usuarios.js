var Usuario = require('../models/usuario');

module.exports = {
    list: function(req, res, next){
        Usuario.find({}, (err, usuarios) => {
            res.render('usuarios/index', {usuarios: usuarios})
        })
    },

    createGet: function(req, res, next){
        res.render('usuarios/create', {errors: {}, usuario: new Usuario()})
    },

    create: function(req, res, next){
        if (req.body.pwd != req.body.confirm_pwd){
            res.render('usuarios/create', {errors: {confirm_pwd: { mensaje: 'Las contraseñas no coinciden'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
            return
        }

        Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.pwd}, (err, nuevoUsuario) => {
            if (err){
                console.log(err);
                res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
            } else {
                nuevoUsuario.enviarMailBienvenida()
                res.redirect('/usuarios');
                return
            }
        })
    },

    updateGet: function(req, res, next){
        Usuario.findById(req.params.id, (err, usuario) => {
            res.render('usuarios/update', {errors: {}, usuario: usuario})
        })
    },

    update: function(req, res, next){
        var updateValues = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, updateValues, (err, usuario) => {
            if (err){
                console.log(err);
                res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
            } else {
                res.redirect('/usuarios');
                return
            }
        })
    },
    
    delete: function(req, res, next){
        Usuario.findOneAndDelete(req.params.id, function(err, cb){
            if (err){
                console.log(err);
                res.render('/usuarios', {errors: err.errors})
            } else {
                res.redirect('/usuarios')
                return
            }
        })
    }
}