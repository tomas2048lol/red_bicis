var Usuario = require('../models/modeloUsuario');
var Token = require('../models/modeloToken');

module.exports = {
    confirmationGet: function(req,res,next){
        Token.findOne({token: req.params.token}, function(err, token){
            if (!token) { return res.status(400).send({ type: "Not-Verified", msg: "Token no valido, quizas ha expirado"}) }
            Usuario.findById(token._userId, function( err, usuario){
                if (!usuario) { return res.status(400).send({type: "Not-Verified", msg: "No encontramos un usuario con este token"}) }
                if (usuario.verificado) { return res.redirect('/usuarios') }
                usuario.verificado = true;
                usuario.save(function(err){
                    if (err) { return res.status(500).send({msg: err.message}); }
                    res.redirect('/')
                })
            })
        })
    }
}