var express = require('express');
var router = express.Router();
var token = require('../controllers/token')

router.get('/confirmation/:token', token.confirmationGet);

module.exports = router;